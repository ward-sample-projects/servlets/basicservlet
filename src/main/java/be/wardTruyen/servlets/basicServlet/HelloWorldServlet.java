package be.wardTruyen.servlets.basicServlet;

//# Tomcat10 imports

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * HelloWorld servlet code (Annotatie niet vergeten voor de registratie in tomcat)
 */
@WebServlet("/HelloWorld")
public class HelloWorldServlet extends HttpServlet {
   @Override
   protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
      String name = req.getParameter("name");
      if (name == null) name = "World";
      System.out.println("Hello " + name + "!");

      resp.setContentType("text/html");
      resp.setCharacterEncoding("UTF-8");
      try (PrintWriter out = resp.getWriter()) {
         out.println("<!DOCTYPE html>");
         out.println("<html lang='en'>");
         out.println("<head>");
         out.println("   <meta charset='utf-8'/>");
         out.println("   <title>HelloWorld servlet</title>");
         out.println("   <link rel='stylesheet' type='text/css' href='css/main.css'>");
         out.println("   <script src='js/stars.js'></script>");
         out.println("</head>");
         out.println("<body>");
         out.println("<main>");

         out.println("   <h1>HelloWorld servlet</h1>");
         out.println("   <p>Hello " + name + "!</p>");
         out.println("   <a href='index.html'>Go back</a>");

         out.println("</main>");
         out.println("<footer>");
         out.println("  <h3>Servlet samples: basicServlet</h3>");
         out.println("  <p class='float-right author'>Author: Ward Truyen</p>");
         out.println("</footer>");
         out.println("</body>");
         out.println("</html>");
      }
   }
}
