const starsStyleLinkId = 'starsCss';
const starsStyleCookie = 'useStarsStyle';

function toggleStars() {
    const el = document.getElementById(starsStyleLinkId);
    if (el) { // if style link does exist, destroy
        el.disabled = true;
        el.remove();

        document.getElementById("stars1").remove();
        document.getElementById("stars2").remove();
        document.getElementById("stars3").remove();
    } else { // if style link does not exist, create
        const head = document.getElementsByTagName('head')[0];
        const link = document.createElement('link');
        link.id = starsStyleLinkId;
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = 'css/stars.css';
        link.media = 'all';
        head.appendChild(link);

        for (let i = 0; i < 3; i++) {
            let star = document.createElement("div");
            star.id = 'stars' + (i + 1);
            //console.log("adding star: " + star.id);
            document.body.appendChild(star);
        }
    }
}

function clickStarsButton() {
    toggleStars();
    if (document.getElementById(starsStyleLinkId)) { // add cookie
        document.cookie = starsStyleCookie + "=true; expires=Fri, 31 Dec 9999 23:59:59 GMT; SameSite=None; Secure";
    } else { // remove cookie
        document.cookie = starsStyleCookie + "=true; expires=Thu, 01 Jan 1970 00:00:00 GMT; SameSite=None; Secure";
    }
    //console.log("cookies: " + document.cookie);
}

function addStarsButton() {
    const btn = document.createElement("button");
    btn.textContent = "Toggle stars";
    btn.onclick = clickStarsButton;
    btn.style.position = 'absolute';
    btn.style.right = '0px';
    document.body.appendChild(btn);
}

window.onload = (event) => {
    addStarsButton();
    //console.log("cookies: " + document.cookie)
    if (document.cookie.split('; ').find((row) => row.startsWith(starsStyleCookie))?.split('=')[1] == "true") {
        toggleStars();
    }
}